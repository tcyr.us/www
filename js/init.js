$(function() {
	$('.sidenav').sidenav();
	$('.scrollspy').scrollSpy();

	particlesJS.load('particles', 'assets/particles.json');
});
